# scigl_render
Library to simplify rendering objects via OpenGL. The intendet use case is 
scientific (e.g. probabalistic filtering).

# dependencies
This package is built with cmake but comes with a package.xml for 
[colcon](https://colcon.readthedocs.io/en/latest/index.html) as it will be used
in ROS2.
The dependencies are handled via [conan.io](https://conan.io/).
Please add the following repositories:
```bash
conan remote add tuebel-gl3w https://api.bintray.com/conan/tuebel/gl3w
conan remote add bincrafters https://api.bintray.com/conan/bincrafters/public-conan
conan remote add conan-transit https://api.bintray.com/conan/conan/conan-transit
```

Conan will download and install the following libraries:
- gl3w (https://github.com/skaslev/gl3w)
- glfw3 (http://www.glfw.org/)
- assimp (http://www.assimp.org/)
- glm (https://glm.g-truc.net/)

Of course a OpenGL driver is required (at least 4.2 since I am using 
glTexStorage2D but it could be replaced with the older glTexImage2D), see
https://www.khronos.org/opengl/wiki/Texture_Storage for more information).

I also built it with CMake. Since the package searches for the ament_cmake
package I suppose you need to have at least ament_cmake installed.

# Notice

If you want to learn more about the OpenGL implementation, please visit Joey de
Vries website https://learnopengl.com/ and his twitter page 
https://twitter.com/JoeyDeVriez. Thanks to his awesome resources I managed to
write this library in a bit more than a week.

# CI
A gitlab CI pipeline is implemented, which is based on [ros_gitlab_ci](https://gitlab.com/VictorLamoine/ros_gitlab_ci). The CI is based on the ros:melodic-ros-core docker image and will install the dependencies via rosdep. The gitlab-runner must use a docker executor.