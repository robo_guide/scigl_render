#pragma once
#include <GL/gl3w.h>
#include <iostream>

namespace scigl_render
{
inline void check_gl_error(const std::string& msg)
{
  GLenum err;
  while ((err = glGetError()) != GL_NO_ERROR)
  {
    std::cerr << "OpenGL error: " << msg << std::hex << err << "\n";
  }
}
} // namespace scigl_render