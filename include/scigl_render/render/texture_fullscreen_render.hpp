#pragma once
#include <memory>
#include <scigl_render/buffer/texture2d.hpp>
#include <scigl_render/shader/shader.hpp>

namespace scigl_render
{
/*!
Allows rendering a texture to a fullscreen quad.
This class ships its own shader to accomplish the task.
*/
class TextureFullscreenRender
{
public:
  /*!
  Create the renderer with a texture storage with the given format.
  \param internal_format the internal format used in glTexStorage2D
  */
  TextureFullscreenRender();

  /**
   * Draws the contents of the texture to a fullscreen quad.
   * Changes glDepthFunc(GL_LEQUAL) so the quad can be seen (z = 1.0).
  */
  void draw(std::shared_ptr<Texture2D> texture) const;

private:
  // Dummy VAO since the geometry shader creates the vertices
  GLuint vao;
  Shader shader;
};
} // namespace scigl_render