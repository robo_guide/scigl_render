#pragma once
#include <scigl_render/scene/cv_camera.hpp>
#include <scigl_render/scene/model.hpp>

namespace scigl_render
{
class DepthSimulator
{
public:
  /**
   * Create a simulator for this camera and model.
  */
  DepthSimulator(scigl_render::CvCamera camera,
                 scigl_render::Model model);

  /**
   * Renders the simulated depth image of the model.
   * \param state the current pose of the object
   */
  void render_pose(const QuaternionPose &object_pose,
                   const QuaternionPose &camera_pose);

  /** width of the simulated images */
  int get_width();
  /** height of the simulated images */
  int get_height();

  // the OpenGL texture needed to render depth values
  static const GLenum FORMAT;
  static const GLenum TYPE;
  static const GLenum INTERNAL_FORMAT;
  static const size_t CHANNELS;
  static const size_t PIXEL_SIZE;

private:
  // where and how to render
  scigl_render::CvCamera camera;
  scigl_render::Shader depth_shader;
  scigl_render::Model model;
};
} // namespace scigl_render