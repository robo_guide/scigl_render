#pragma once
#include <scigl_render/gl_context.hpp>
#include <scigl_render/render/depth_simulator.hpp>
#include <scigl_render/buffer/frame_buffer.hpp>
#include <scigl_render/buffer/texture_reader.hpp>
#include <scigl_render/render/rasterizer.hpp>
#include <scigl_render/render/texture_fullscreen_render.hpp>
#include <memory>
#include <vector>

namespace scigl_render
{
/*!
Demonstrates how to render a scene with this library. Frankly this is quite
the unnecessary complicated case, since I test rendering offscreen to a FBO,
transfer the texture to a PBO and reading this into CPU memory. Then I push it
back to the GPU as anoter texture generated from the CPU memory and it will is
be displayed on screen.
*/
class DepthOffscreenRender
{
public:
  /*!
  Configures the rendering environment and loads the models.
  \param context the environment to render to (swap Buffers)
  \param texture parametrized for offscreen rendering
  \param pixel_size the size of one pixel. Must match the internal format:
  number_of_channels * sizeof(type)
  */
  DepthOffscreenRender(std::shared_ptr<GLContext> context,
                       std::shared_ptr<Texture2D>,
                       size_t pixel_size);

  /*!
  Continues to render the next frame.
  */
  void next_frame(std::shared_ptr<DepthSimulator> depth_simulator,
                  const CartesianPose &model_pose,
                  const CartesianPose &camera_pose);

private:
  // parameters for rendering
  std::shared_ptr<GLContext> gl_context;
  // render to fbo
  std::shared_ptr<Texture2D> fbo_texture;
  std::shared_ptr<FrameBuffer> framebuffer;
  std::unique_ptr<TextureReader> tex_reader;
  // display fullscreen quad from other texture (to showcase the mapping)
  TextureFullscreenRender texture_render;
  std::shared_ptr<Texture2D> quad_texture;
  // display different views
  Rasterizer rasterizer;
  // CPU memory buffer for reading from the offscreen render
  std::vector<uint8_t> image_buffer;

  /*!
  Display the data from the framebuffer
  */
  void display_data(const void *data);
};
} // namespace scigl_render