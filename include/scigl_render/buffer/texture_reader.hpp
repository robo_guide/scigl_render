#pragma once
#include <scigl_render/buffer/texture2d.hpp>
#include <array>
#include <functional>
#include <memory>

namespace scigl_render
{
/*!
Transfer data from a texture to the CPU.
Two pixel buffer objects are used like a double buffer: one PBO is filled
asynchronously with data from the frame buffer while the other one is ready to
be mapped to the clients memory. Consequently the results of a rendering is
delayed by one read call. 
*/
class TextureReader
{
public:
  /*!
  Renders the scene off-screen and calculates the depth values.
  \param buffer read from this texture
  \param pixel_size the size of each pixel: number_channels * sizeof(type)
  */
  TextureReader(std::shared_ptr<Texture2D> buffer,
                size_t pixel_size);

  /*!
  Starts reading from the FBO to the backbuffer PBO. This operation is 
  asynchronous.
  */
  void start_read() const;

  /*!
  Reads synchronously from the frontbuffer PBO. Operate on this data while
  another read is running on the backbuffer :)
  \retuns might return NULL otherwise the data
  */
  void *do_read() const;

  /*!
  Unmaps the OpenGL memory.
  */
  void end_read() const;

  /*! Swap the front- and backbuffer */
  void swap_buffers();

private:
  std::shared_ptr<Texture2D> texture;
  // Two pbos one to read to the other one to map form, alternating
  std::array<GLuint, 2> pbos;
  // transfer from fbo to pbo via glReadPixels
  int backbuffer_index = 0;
  // transfer from pbo to CPU memory
  int frontbuffer_index = 1;
};
} // namespace scigl_render
