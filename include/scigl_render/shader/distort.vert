R""(
#version 330 core
layout (location = 0) in vec3 position;
layout (location = 1) in vec3 normal_in;
layout (location = 2) in vec2 texture_coordinate_in;

uniform float dist_coeffs[8];
uniform mat4 model_matrix;
uniform mat4 projection_matrix;
uniform mat4 view_matrix;

out float depth;
out vec3 normal;
out vec2 texture_coordinate;
out vec3 world_position;

// distort the real world vertices using the rational model
vec4 distort(vec4 view_pos)
{
  // normalize
  float z = view_pos[2];
  float z_inv = 1 / z;
  float x1 = view_pos[0] * z_inv;
  float y1 = view_pos[1] * z_inv;
  // precalculations
  float x1_2 = x1*x1;
  float y1_2 = y1*y1;
  float x1_y1 = x1*y1;
  float r2 = x1_2 + y1_2;
  float r4 = r2*r2;
  float r6 = r4*r2;
  // rational distortion factor
  float r_dist = (1 + dist_coeffs[0]*r2 +dist_coeffs[1]*r4 + dist_coeffs[4]*r6) 
    / (1 + dist_coeffs[5]*r2 + dist_coeffs[6]*r4 + dist_coeffs[7]*r6);
  // full (rational + tangential) distortion
  float x2 = x1*r_dist + 2*dist_coeffs[2]*x1_y1 + dist_coeffs[3]*(r2 + 2*x1_2);
  float y2 = y1*r_dist + 2*dist_coeffs[3]*x1_y1 + dist_coeffs[2]*(r2 + 2*y1_2);
  // denormalize for projection (which is a linear operation)
  return vec4(x2*z, y2*z, z, view_pos[3]);
}


// transforms the model vertices and normals to world coordinates
void main()
{
  // transform to world
  vec4 local_position = vec4(position, 1.0);
  world_position  =  (model_matrix * local_position).xyz;
  normal = normalize(mat3(transpose(inverse(model_matrix))) * normal_in);
  texture_coordinate = texture_coordinate_in;
  // distortion in view coordinates
  vec4 view_pos = view_matrix * vec4(world_position, 1.0);
  vec4 dist_pos = distort(view_pos);
  depth = view_pos.z;
  gl_Position = projection_matrix * dist_pos;
}
)""