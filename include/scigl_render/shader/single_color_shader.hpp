#pragma once
#include <scigl_render/shader/shader.hpp>

namespace scigl_render
{
/*!
This shader renders a model in a single color.
The color results from the uniform light_color and object_color.
*/
class SingleColorShader
{
public:
  /*!
  Compiles a shader program for rendering single color objects.
  */
  static Shader create_shader();

  /*!
  Compiles a shader program for rendering single color objects.
  \param ambient_strength between 0 (not ambient light) to 1 (super bright)
  \param r red element of the RGB color, ranges from 0 to 1
  \param g green element of the RGB color, ranges from 0 to 1
  \param b blue element of the RGB color, ranges from 0 to 1
  */
  static Shader create_shader(float ambient_strength,
                              float r, float g, float b);

  /*!
  Set the strength of the (undirected) ambient light.
  \param shader this shader will use the ambient_strength
  \param ambient_strength between 0 (not ambient light) to 1 (super bright)
  */
  static void set_ambient_strength(const Shader &shader,
                                   float ambient_strength);

  /*!
  Set the color which is used to render the mesh in the shader.
  \param shader this shader will use the object_color
  \param r red element of the RGB color, ranges from 0 to 1
  \param g green element of the RGB color, ranges from 0 to 1
  \param b blue element of the RGB color, ranges from 0 to 1
  */
  static void set_color(const Shader &shader, float r, float g, float b);
};
} // namespace scigl_render