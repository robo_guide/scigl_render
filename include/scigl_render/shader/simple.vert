R""(
#version 330 core
layout (location = 0) in vec3 position;

uniform mat4 model_matrix;
uniform mat4 view_matrix;
uniform mat4 projection_matrix;

out float depth;
out vec3 world_position;

// simple linear transformation into projection space
void main()
{
  vec4 local_position = vec4(position, 1.0);
  world_position = (model_matrix * local_position).xyz;
  vec4 view_pos  = view_matrix * vec4(world_position, 1.0);
  depth = view_pos.z;
  gl_Position = projection_matrix * view_pos;
})""