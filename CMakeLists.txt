cmake_minimum_required(VERSION 2.8.3)
project(scigl_render)

set(CMAKE_CXX_STANDARD 11)
set(SCIGL_RENDER_VERSION 0.6.1)

if(CMAKE_COMPILER_IS_GNUCXX OR CMAKE_CXX_COMPILER_ID MATCHES "Clang")
  add_compile_options(-Wall -Wextra -Wpedantic)
endif()

find_package(gl3w REQUIRED)
find_package(assimp REQUIRED)
find_package(glfw3 REQUIRED)
find_package(glm REQUIRED)

# prefer new ABI for OpenGL
set(OpenGL_GL_PREFERENCE "GLVND")
# -fPIC
set(CMAKE_POSITION_INDEPENDENT_CODE ON)

# use GNUInstalDirs for CMAKE_INSTALL_ variables
include(GNUInstallDirs)

# the core library
set(scigl_render_INCLUDE_DIRS include)
add_library(scigl_render
  src/gl_context.cpp
  src/buffer/frame_buffer.cpp
  src/buffer/frame_buffer_reader.cpp
  src/buffer/texture2d.cpp
  src/buffer/texture_reader.cpp
  src/render/depth_simulator.cpp
  src/render/onscreen_render.cpp
  src/render/rasterizer.cpp
  src/render/texture_fullscreen_render.cpp
  src/scene/cv_camera.cpp
  src/scene/diffuse_light.cpp
  src/scene/mesh.cpp  
  src/scene/model.cpp
  src/shader/shader.cpp
  src/shader/shader_builder.cpp
  src/shader/single_color_shader.cpp)
target_include_directories(scigl_render PUBLIC
  $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/${scigl_render_INCLUDE_DIRS}>
  $<INSTALL_INTERFACE:${CMAKE_INSTALL_INCLUDEDIR}>)
target_link_libraries(scigl_render
  PUBLIC assimp glfw glm gl3w)

# example applications
add_executable(scigl_viewer
  src/example/scigl_viewer.cpp)
target_link_libraries(scigl_viewer
  scigl_render)

add_executable(scigl_depth_viewer
  src/example/scigl_depth_viewer.cpp
  src/example/depth_offscreen_render.cpp)
target_link_libraries(scigl_depth_viewer
  scigl_render)

# install
install(TARGETS scigl_render scigl_viewer scigl_depth_viewer
  EXPORT scigl_renderTargets
  ARCHIVE DESTINATION ${CMAKE_INSTALL_LIBDIR}
  LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR}
  RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR}
  INCLUDES DESTINATION ${CMAKE_INSTALL_INCLUDEDIR})
INSTALL(
  DIRECTORY include/scigl_render
  DESTINATION ${CMAKE_INSTALL_INCLUDEDIR})

# CMake package
include(CMakePackageConfigHelpers)
write_basic_package_version_file(
  ${CMAKE_CURRENT_BINARY_DIR}/scigl_renderConfigVersion.cmake
  VERSION ${SCIGL_RENDER_VERSION}
  COMPATIBILITY SameMajorVersion)

export(EXPORT scigl_renderTargets
  FILE ${CMAKE_CURRENT_BINARY_DIR}/scigl_renderTargets.cmake
  NAMESPACE scigl_render::)

set(ConfigPackageLocation ${CMAKE_INSTALL_DATAROOTDIR}/scigl_render/cmake)
configure_package_config_file(
  scigl_renderConfig.cmake.in
  ${CMAKE_CURRENT_BINARY_DIR}/scigl_renderConfig.cmake
  INSTALL_DESTINATION ${ConfigPackageLocation}
  PATH_VARS scigl_render_INCLUDE_DIRS)

install(EXPORT scigl_renderTargets
  FILE
    scigl_renderTargets.cmake
  NAMESPACE
    scigl_render::
  DESTINATION
    ${ConfigPackageLocation})
install(
  FILES
    ${CMAKE_CURRENT_BINARY_DIR}/scigl_renderConfig.cmake
    ${CMAKE_CURRENT_BINARY_DIR}/scigl_renderConfigVersion.cmake
  DESTINATION
    ${ConfigPackageLocation})
