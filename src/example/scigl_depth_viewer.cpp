#include <glm/glm.hpp>
#include <stdexcept>
#include <iostream>
#include <scigl_render/check_gl_error.hpp>
#include <scigl_render/gl_context.hpp>
#include <scigl_render/example/depth_offscreen_render.hpp>
#include <scigl_render/render/depth_simulator.hpp>
#include <scigl_render/scene/cv_camera.hpp>
#include <scigl_render/scene/diffuse_light.hpp>

// forward declare callbacks
// get mouse and keyboard inputs
void process_input(GLFWwindow *window,
                   scigl_render::CartesianPose &camera_pose);

// Debug printing
std::ostream &operator<<(std::ostream &out, const glm::mat4 &mat)
{
  out << "[" << mat[0][0] << " " << mat[1][0] << " " << mat[2][0] << " " << mat[3][0] << "\n"
      << mat[0][1] << " " << mat[1][1] << " " << mat[2][1] << " " << mat[3][1] << "\n"
      << mat[0][2] << " " << mat[1][2] << " " << mat[2][2] << " " << mat[3][2] << "\n"
      << mat[0][3] << " " << mat[1][3] << " " << mat[2][3] << " " << mat[3][3] << "]";

  return out;
}

/*!
Provide the model as argument via command line.
*/
int main(int argc, char *argv[])
{
  // Some default parameters
  const int WIDTH = 640;
  const int HEIGHT = 480;
  using namespace scigl_render;
  if (argc < 2)
  {
    throw std::runtime_error(
        "No model file provided. Run as */scigl_viwer <model_filename>!");
  }
  // Setup renderer creates context
  auto context = std::make_shared<GLContext>(true, false, WIDTH, HEIGHT);
  check_gl_error("Context created");
  auto texture = std::make_shared<Texture2D>(
      WIDTH, HEIGHT, GL_RED, GL_R32F, GL_FLOAT);
  check_gl_error("texture created");
  DepthOffscreenRender render(context, texture, sizeof(float));
  // Intrinsics of my shitty webcam
  CameraIntrinsics camera_intrinsics;
  camera_intrinsics.width = 640;
  camera_intrinsics.height = 480;
  camera_intrinsics.c_x = 411;
  camera_intrinsics.c_y = 310;
  camera_intrinsics.f_x = 511;
  camera_intrinsics.f_y = 513;
  float dist_coeffs[] = {
      4.1925421198910247e-02,
      -9.6463442423611379e-02,
      -2.3391717576772839e-03,
      5.8792609967242386e-04,
      4.9171950039135250e-02,
      0, 0, 0};
  std::copy(std::begin(dist_coeffs), std::end(dist_coeffs),
            std::begin(camera_intrinsics.dist_coeffs));
  // create the depth simulator
  auto depth_simulator = std::make_shared<DepthSimulator>(
      CvCamera(camera_intrinsics), Model(argv[1]));
  check_gl_error("Built depth shader");
  // Test if Cartesian -> Quaternion works
  CartesianPose camera_pose(glm::vec3(0, 0, 0), glm::vec3(0, 0, 0));
  CartesianPose model_pose(glm::vec3(0, 0, 1), glm::vec3(0, 0, 0));
  // main loop
  while (!glfwWindowShouldClose(context->get_window()))
  {
    process_input(context->get_window(), camera_pose);
    render.next_frame(depth_simulator, model_pose, camera_pose);
    glfwPollEvents();
  }
  // Finally release the context
  glfwTerminate();
  return EXIT_SUCCESS;
}

void process_input(GLFWwindow *window,
                   scigl_render::CartesianPose &camera_pose)
{
  // exit
  if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
  {
    glfwSetWindowShouldClose(window, true);
  }
  // movement
  float cameraSpeed = 0.005;
  if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
  {
    camera_pose.position.y += cameraSpeed;
  }
  if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
  {
    camera_pose.position.y -= cameraSpeed;
  }
  if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
  {
    camera_pose.position.x -= cameraSpeed;
  }
  if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
  {
    camera_pose.position.x += cameraSpeed;
  }
  if (glfwGetKey(window, GLFW_KEY_UP) == GLFW_PRESS)
  {
    camera_pose.position.z += cameraSpeed;
  }
  if (glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_PRESS)
  {
    camera_pose.position.z -= cameraSpeed;
  }
  if (glfwGetKey(window, GLFW_KEY_I) == GLFW_PRESS)
  {
    camera_pose.orientation.x += cameraSpeed;
  }
  if (glfwGetKey(window, GLFW_KEY_K) == GLFW_PRESS)
  {
    camera_pose.orientation.x -= cameraSpeed;
  }
  if (glfwGetKey(window, GLFW_KEY_J) == GLFW_PRESS)
  {
    camera_pose.orientation.y -= cameraSpeed;
  }
  if (glfwGetKey(window, GLFW_KEY_L) == GLFW_PRESS)
  {
    camera_pose.orientation.y += cameraSpeed;
  }
  if (glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS)
  {
    camera_pose.orientation.z -= cameraSpeed;
  }
  if (glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS)
  {
    camera_pose.orientation.z += cameraSpeed;
  }
}
