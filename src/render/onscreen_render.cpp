#include <scigl_render/check_gl_error.hpp>
#include <scigl_render/render/onscreen_render.hpp>

namespace scigl_render
{
OnscreenRender::OnscreenRender() : OnscreenRender(Shader(0)) {}

OnscreenRender::OnscreenRender(Shader shader)
{
  // Configure the global rendering settings
  glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);
  set_shader_program(std::move(shader));
}

void OnscreenRender::next_frame(
    GLContext &gl_context, const CvCamera &camera,
    const Model &model, const DiffuseLight &light)
{
  glClearColor(0, 0, 0, 0);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  glEnable(GL_DEPTH_TEST);
  camera.set_in_shader(shader);
  light.set_in_shader(shader);
  shader.activate();
  model.draw(shader);
  shader.deactivate();
  glfwSwapBuffers(gl_context.get_window());
  check_gl_error("rendered next frame");
}

void OnscreenRender::set_shader_program(Shader shader)
{
  this->shader = std::move(shader);
}

} // namespace scigl_render