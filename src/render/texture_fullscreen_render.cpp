#include <scigl_render/check_gl_error.hpp>
#include <scigl_render/render/texture_fullscreen_render.hpp>
#include <scigl_render/shader/shader_builder.hpp>

namespace scigl_render
{
const std::string vertex_source = R"(
#version 330 core

void main()
{
})";
const std::string geometry_source = 
#include <scigl_render/shader/fullscreen_quad.geom>
"";
const std::string fragment_source = R"(
#version 330 core 
in vec2 texture_coordinate;
uniform sampler2D texture0;
out vec4 color;

void main()
{ 
  color = texture(texture0, texture_coordinate);
})";

TextureFullscreenRender::TextureFullscreenRender()
{
  ShaderBuilder shader_builder;
  shader_builder.attach_vertex_shader(vertex_source);
  shader_builder.attach_geometry_shader(geometry_source);
  shader_builder.attach_fragment_shader(fragment_source);
  shader = shader_builder.build();
  // create dummy VAO
  glGenVertexArrays(1, &vao);
  glBindVertexArray(vao);
  glBindVertexArray(0);
  check_gl_error("TextureFullscreenRender created");
}

void TextureFullscreenRender::draw(std::shared_ptr<Texture2D> texture) const
{
  // With GL_LESS the cleared buffer (z = 1.0) would always win
  glDepthFunc(GL_LEQUAL);

  shader.activate();
  texture->bind();
  Texture2D::activate(GL_TEXTURE0);
  shader.setInt("texture0", 0);
  glBindVertexArray(vao);
  glDrawArrays(GL_POINTS, 0, 1);
  // unbind the resources
  shader.deactivate();
  texture->unbind();
  glBindVertexArray(0);
}
} // namespace scigl_render