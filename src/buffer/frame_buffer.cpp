#include <scigl_render/buffer/frame_buffer.hpp>
#include <stdexcept>

namespace scigl_render {
FrameBuffer::FrameBuffer(std::shared_ptr<Texture2D> texture)
    : color_tex(texture) {
  // Create framebuffer with renderbuffer attachements
  glGenFramebuffers(1, &fbo);
  activate();
  color_tex->bind();
  glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D,
                         color_tex->get_raw(), 0);
  color_tex->unbind();
  glGenRenderbuffers(1, &depth_stencil_rbo);
  glBindRenderbuffer(GL_RENDERBUFFER, depth_stencil_rbo);
  glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8,
                        color_tex->get_width(), color_tex->get_height());
  glBindRenderbuffer(GL_RENDERBUFFER, 0);
  glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT,
                            GL_RENDERBUFFER, depth_stencil_rbo);
  // Check framebuffer, will check rbo, too, since it is attached
  if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
    throw std::runtime_error("Framebuffer is not completed");
  }
  deactivate();
}

FrameBuffer::~FrameBuffer() {
  glBindFramebuffer(GL_FRAMEBUFFER, 0);
  glDeleteRenderbuffers(1, &depth_stencil_rbo);
}

void FrameBuffer::activate() const { glBindFramebuffer(GL_FRAMEBUFFER, fbo); }

void FrameBuffer::deactivate() const { glBindFramebuffer(GL_FRAMEBUFFER, 0); }

void FrameBuffer::clear(float color, float depth, int stencil) const {
  activate();
  glClearColor(color, color, color, 1);
  glClearDepth(depth);
  glClearStencil(stencil);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
  deactivate();
}

std::shared_ptr<Texture2D> FrameBuffer::get_texture() const {
  return color_tex;
}

int FrameBuffer::get_max_size() {
  int max_rbo_size;
  glGetIntegerv(GL_MAX_RENDERBUFFER_SIZE, &max_rbo_size);
  return max_rbo_size;
}

void FrameBuffer::read_image(GLvoid *pixels, GLint x, GLint y, GLsizei width,
                             GLsizei height) const {
  activate();
  glReadPixels(x, y, width, height, color_tex->get_format(),
               color_tex->get_type(), pixels);
  deactivate();
}
}  // namespace scigl_render