#include <scigl_render/buffer/texture2d.hpp>

namespace scigl_render {
Texture2D::Texture2D(GLsizei width, GLsizei height, GLenum format,
                     GLenum internal_format, GLenum type)
    : width(width),
      height(height),
      format(format),
      internal_format(internal_format),
      type(type) {
  glGenTextures(1, &texture);
  bind();
  glTexStorage2D(GL_TEXTURE_2D, 1, internal_format, width, height);
  unbind();
}

Texture2D::~Texture2D() { glDeleteTextures(1, &texture); }

GLuint Texture2D::get_raw() const { return texture; }

GLsizei Texture2D::get_width() const { return width; }
GLsizei Texture2D::get_height() const { return height; }
GLenum Texture2D::get_format() const { return format; }
GLenum Texture2D::get_internal_format() const { return internal_format; }
GLenum Texture2D::get_type() const { return type; }

void Texture2D::activate(GLenum texture_n) { glActiveTexture(texture_n); }

void Texture2D::bind() const { glBindTexture(GL_TEXTURE_2D, texture); }

void Texture2D::bind_image_unit(GLuint unit, GLenum access) const {
  glBindImageTexture(unit, texture, 0, GL_FALSE, 0, access, internal_format);
}

void Texture2D::unbind() const { glBindTexture(GL_TEXTURE_2D, 0); }

void Texture2D::read_image(GLvoid *pixels) const {
  bind();
  glGetTexImage(GL_TEXTURE_2D, 0, format, type, pixels);
  unbind();
}

void Texture2D::store_image(const GLvoid *image) const {
  bind();
  glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, width, height, format, type, image);
  unbind();
}
}  // namespace scigl_render
